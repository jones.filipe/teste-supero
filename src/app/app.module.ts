import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TasksComponent } from './main/tasks/tasks.component';
import { AdicionarTaskComponent } from './main/adicionar-task/adicionar-task.component';
import { ExibirTaskComponent } from './main/exibir-task/exibir-task.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/pt';
import { ToastComponent } from './commons/toast/toast.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from './services/toast.service';
import { NotificacaoService } from './services/notificacao.service';
registerLocaleData(pt)


@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    AdicionarTaskComponent,
    ExibirTaskComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' },
    ToastService,
    NotificacaoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
