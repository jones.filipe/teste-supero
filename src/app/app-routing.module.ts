import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdicionarTaskComponent } from './main/adicionar-task/adicionar-task.component';
import { ExibirTaskComponent } from './main/exibir-task/exibir-task.component';
import { TasksComponent } from './main/tasks/tasks.component';


const routes: Routes = [
  { path: '', redirectTo: 'tasks', pathMatch: 'full' },
  { path: 'tasks', component: TasksComponent },
  { path: 'tasks/:id', component: ExibirTaskComponent },
  { path: 'adicionar-task', component: AdicionarTaskComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
