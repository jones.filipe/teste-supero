import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Task } from '../model/task';



@Injectable({
  providedIn: 'root'
})
export class TaskService {

  url: string

  constructor(private http: HttpClient) {
    this.url = `${environment.apiUrl}/`;
   }

   getTasks() : Observable<any> {
    return this.http.get(this.url);
  }

  salvar(task: Task) : Observable<any> {
    return this.http.post(this.url, task);
  }

  findTask(id: number) : Observable<any> {
    return this.http.get(this.url + id)
  }

  remover(id: number) : Observable<any> {
    return this.http.delete(this.url + id);
  }

  atualizarStatus(id: number) : Observable<any> {
    return this.http.get(this.url + 'atualizar-status/' + id)
  }
}
