// toast.service.ts
import { Injectable, TemplateRef  } from '@angular/core';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class NotificacaoService {

    constructor(
        public toastService: ToastService
      ) {}

    sucesso(mensagem: string) {
        this.toastService.show(mensagem, {
          classname: 'bg-success text-light',
          delay: 2000 ,
          autohide: true,
        });
      }

      erro(mensagem: string) {
        this.toastService.show(mensagem, {
          classname: 'bg-danger text-light',
          delay: 2000 ,
          autohide: true,
        });
      }
}