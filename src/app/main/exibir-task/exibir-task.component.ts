import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacaoService } from 'src/app/services/notificacao.service';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-exibir-task',
  templateUrl: './exibir-task.component.html',
  styleUrls: ['./exibir-task.component.css']
})
export class ExibirTaskComponent implements OnInit {

  taskSelecionada = null;

  constructor(
    private taskService: TaskService,
    private notificacaoService: NotificacaoService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.getTask(this.route.snapshot.paramMap.get('id'));
  }

  getTask(id) {
    this.taskService.findTask(id)
      .subscribe(
        data => {
          this.taskSelecionada = data;
        },
        error => {
          this.notificacaoService.erro("Erro ao buscar task");
        });
  }

  atualizarStatus() {
    this.taskService.atualizarStatus(this.taskSelecionada.id)
      .subscribe(
        data => {
          this.taskSelecionada = data;
          this.notificacaoService.sucesso("Status alterado com sucesso")
        }, error => {
          this.notificacaoService.erro("Erro ao alterar status");
        }
      )
  }

  editarTask() {
    this.taskService.salvar(this.taskSelecionada)
      .subscribe(
        response => {
          this.notificacaoService.sucesso("Task editada com sucesso")
          this.router.navigate(['/tasks']);
        },
        error => {
          this.notificacaoService.erro("Erro ao editar task")
        });
  }

}
