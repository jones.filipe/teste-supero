import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExibirTaskComponent } from './exibir-task.component';

describe('ExibirTaskComponent', () => {
  let component: ExibirTaskComponent;
  let fixture: ComponentFixture<ExibirTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExibirTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExibirTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
