import { Component, OnInit } from '@angular/core';
import { NotificacaoService } from 'src/app/services/notificacao.service';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks: any[];
  taskSelecionada = null;
  indiceSelecionado = -1;

  constructor(private taskService: TaskService,
              private notificacaoService: NotificacaoService) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.taskService.getTasks()
      .subscribe(
        data => {
          this.tasks = data;
        },
        error => {
          this.notificacaoService.erro("Erro ao buscar as tasks")
        });
  }

  removerTask() {
    this.taskService.remover(this.taskSelecionada.id)
      .subscribe(
        response => {
          this.tasks = this.tasks.filter(task => task.id !== this.taskSelecionada.id);
          this.taskSelecionada = null;
          this.notificacaoService.sucesso("Task excluída com sucesso")
        },
        error => {
          this.notificacaoService.erro("Erro ao remover task")
        });
  }

  selecionaTask(task, indice) {
    this.taskSelecionada = task;
    this.indiceSelecionado = indice;
  }

}
