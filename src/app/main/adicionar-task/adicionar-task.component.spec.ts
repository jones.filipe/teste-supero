import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarTaskComponent } from './adicionar-task.component';

describe('AdicionarTaskComponent', () => {
  let component: AdicionarTaskComponent;
  let fixture: ComponentFixture<AdicionarTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
