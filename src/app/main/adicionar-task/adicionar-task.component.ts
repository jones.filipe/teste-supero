import { TmplAstElement } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/model/task';
import { NotificacaoService } from 'src/app/services/notificacao.service';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-adicionar-task',
  templateUrl: './adicionar-task.component.html',
  styleUrls: ['./adicionar-task.component.css']
})

export class AdicionarTaskComponent implements OnInit {
  task: Task = new Task();
  taskSalva: boolean = false;

  constructor(private taskService: TaskService,
              private notificacaoService: NotificacaoService) { }

  ngOnInit() {
  }

  salvar() {
    this.taskService.salvar(this.task)
      .subscribe(
        response => {
          if(response) {
            this.notificacaoService.sucesso("Task salva com sucesso")
          }
          this.taskSalva = true;
        },
        error => {
          this.notificacaoService.erro("Erro ao salvar task")
        });
  }

  novaTask() {
    this.task = new Task();
    this.taskSalva = false
  }


}
