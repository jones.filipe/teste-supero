export class Task {
    titulo: string;
    descricao: string;
    dataCriacao: Date;
    dataAtualizacao: Date
    concluido: boolean;
}